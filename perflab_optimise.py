# ENGSCI233: Lab - Performance
# perflab_optimise.py

# PURPOSE:
# Assessed exercises for optimising the LU factorisation algorithm.

# PREPARATION:
# Work through the exercises in perflab_practice.py.

# SUBMISSION:
# - YOU MUST submit this file to complete the lab. 
# - DO NOT change the file name.
# - You should ALSO SUBMIT perflab_scaling.png from EXERCISE 1

# TO DO:
# - COMPLETE Exercises 1 through 4.

# imports
from perflab_functions import*
import cProfile, pstats
from multiprocessing import Pool
from glob import glob
from time import time

if __name__ == "__main__":
    
    ##############################################################################################
    # EXERCISE 1: Quantify scaling of lu_factor
    # In this section, you will study how lu_factor performs on larger and larger matrices. 
    # Note, do this exercise BEFORE optimising lu_factor (EXERCISE 3)
    #
    # TO DO:
    #  1. RUN lu_factor for square matrices of size 2^N where N ranges from 5 to 9 
    #  2. PLOT the scaling of lu_factor runtime in log-space
    #  3. SAVE your plot as perflab_scaling.png (use the plot_scaling() function from practice)
    #     (Do this BEFORE Exercise 2 below)
    #  4. ANSWER in perflab_questions.txt:
    #     WHAT is the scaling of LU factorisation in Big O notation?
    
    if False:        
        # **your scaling and plotting code here**
        ts = []                                                # an empty list to store times
        Ns = [2**i for i in range(5,9)]                        # matrix sizes to test
        for N in Ns:                                           # loop over all problem sizes
            A = square_matrix_rand(N)
            cProfile.run('matmult2(A,A)', 'restats')
            p = pstats.Stats('restats')
            t = p.total_tt                             

            # save the execution time
            ts.append(t)

        # **it is fine to reuse the plotting function from perflab_practice.py**
        plot_scaling(np.log10(Ns), np.log10(ts), fit_line=True, save='perflab_scaling.png')
        

    ##############################################################################################
    # EXERCISE 2: Profile lu_factor
    # In this section, you will identify the inefficiencies in lu_factor()
    #
    # TO DO:
    #  1. WRITE code to profile the function lu_factor for the square matrix below
    #  2. In perflab_questions.txt:
    #     EXPLAIN how the printout indicates that row_reduction() is the principle bottleneck

    if False: 
        N = 200
        A = square_matrix_rand(N)

        print('\n Profiling for lu_factor function \n ')  
        cProfile.run('A = lu_factor(A)', 'restats')
        p = pstats.Stats('restats')
        p.sort_stats('time').print_stats(9)

    
    ##############################################################################################
    # EXERCISE 3: Optimise lu_factor
    # In this section, you will fix the inefficiencies in lu_factor().  
    #
    # TO DO:
    #  1. OPTIMISE lu_factor() by vectorising the inner FOR loop in row_reduction()
    #  2. In perflab_questions.txt:
    #     QUANTIFY the speed-up of lu_factor() (to the nearest order of magnitude)

    if False: 
        N = 200
        A = square_matrix_rand(N)
        print('\n Profiling for lu_factor function \n ')  
        # **your profiling code here**
        cProfile.run('A = lu_factor(A)', 'restats')
        p = pstats.Stats('restats')
        p.sort_stats('time').print_stats(9)
    
    ##############################################################################################
    # EXERCISE 4: Parallel application of lu_factor
    # matrices.zip contains 1000 randomly sized matrices that need to be factorised
    # the code below reads the FIRST TWENTY matrices one at a time and factorises them 
    #
    # TO DO:
    #  1. EXTRACT the contents of 'matrices.zip' to the directory 'matrices'
    #  2. RUN the serial code below. In perflab_questions.txt, answer:
    #     HOW LONG does it take to factorise all the matrices? 
    #  3. PARALLELISE this factorisation task, making use of Pool, Pool.map, and 
    #     read_and_factor() (to be completed in perflab_functions.py). 
    #     In perflab_questions.txt, answer: What speedup can you achieve?
    #
    # NOTES:
    #  - DO NOT parallelise the lu_factor function
    #  - the code below makes use of 'glob' and 'np.genfromtxt' - you may wish to look up how these
    #    work, but you won't need to know how to use them until the Data module

    if True:
        # SERIAL calculation
        n = 1000
        fls = glob("matrices/*.txt")[:n]             # 'glob' uses wildcards ('*') to detect files
        t0 = time()                                 #     conforming to a naming pattern
        for fl in fls:
            A = np.genfromtxt(fl,delimiter=',')     # read
            LU = lu_factor(A)                        # factor
        t1 = time()
        serial_time = t1-t0
        print('time to factorise {:d} matrices in SERIAL: {:2.1f} seconds'.format(n,serial_time))

        # PARALLEL calculation
        n = 1000
        fls = glob("matrices/*.txt")[:n]
        ncpus = 2
        p = Pool(ncpus)
        t0 = time()
        outputs = p.map(read_and_factor, fls)
        t1 = time()
        parallel_time = t1-t0
        print('time to factorise {:d} matrices in PARALLEL: {:2.1f} seconds'.format(n,parallel_time))        

        speedup = serial_time/parallel_time
        print('total speedup achieved by parallelisation: {:2.1f}'.format(n,speedup)) 
        
